import {GinBotController} from "./controller/GinBotController";

export const Routes = [{
    method: "get",
    route: "/api/legends",
    controller: GinBotController,
    action: "getLegends"
}];