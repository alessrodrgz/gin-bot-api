import "reflect-metadata";
import * as express from "express";
import * as bodyParser from "body-parser";
import {Request, Response} from "express";
import {Routes} from "./routes";
import * as morgan from "morgan";
import {logger} from "./controller/Logger";
import config from "./config/environment";


const app = express();
app.use(bodyParser.json());
app.use(morgan('dev'));

Routes.forEach(route => {
    (app as any)[route.method](route.route, (req: Request, res: Response, next: Function) => {
        const result = (new (route.controller as any))[route.action](req, res, next);
        if (result instanceof Promise) {
            result.then(result => result !== null && result !== undefined ? res.send(result) : undefined);

        } else if (result !== null && result !== undefined) {
            res.json(result);
        }
    });
});

app.listen(3000);

logger.debug(`Listening on port ${config.port}.`);
