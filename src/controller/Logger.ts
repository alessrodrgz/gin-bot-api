import {$log} from "@tsed/logger";
$log.level = "debug";
$log.name = "DT";

export const logger = $log;
