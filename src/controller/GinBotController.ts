import {Request, Response} from "express";
import {PythonShell} from "python-shell";
import {logger} from "./Logger";

export class GinBotController {

    private pyshell : PythonShell = undefined;

    async getLegends(req: Request, res: Response){
        this.pyshell = new PythonShell(`${__dirname}\\scripts\\update-legends.py`);

        let result : string;

        this.pyshell.on("message", message => {
            if(message.match(/^(http|https):\/\/i.imgur.com\/.*.png$/)) result = message;
        });

        this.pyshell.end(err => {
            if(err) logger.error(err);
            if(result !== undefined) res.send({"url": result});
            else res.status(400).send({"error": "Could not process the image"});
        });
    }
}