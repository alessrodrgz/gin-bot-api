from selenium import webdriver
from PIL import Image
from selenium.webdriver.chrome.options import Options
import time
from os import path
import os
from imgur_python import Imgur

imgur_id = "24e37fed85be6d1"
imgur_secret = "2cab2312f3b0be784f489bf9914d028c8a46d041"

imgur_client = Imgur({'client_id': imgur_id, 'access_token': imgur_secret})

def generate_image():
    website = 'https://optc-legends.github.io/'

    chrome_options = Options()
    chrome_options.add_argument('--no-sandbox')
    chrome_options.add_argument('--disable-dev-shm-usage')
    chrome_options.add_argument('window-size=1920,1080')

    driver = webdriver.Chrome("C:/Users/Nozomu/WebstormProjects/gin-bot-api/src/controller/scripts/chromedriver.exe", options=chrome_options)
    driver.get(website)

    select_all_btn = driver.find_element_by_xpath('//*[@id="select-all"]')
    hide_evos_btn = driver.find_element_by_xpath('//*[@id="hide-base"]')

    select_all_btn.click()
    hide_evos_btn.click()

    generate_image_btn = driver.find_element_by_xpath('//*[@id="generate"]')
    generate_image_btn.click()

    time.sleep(3)

    img = driver.find_element_by_xpath('/html/body/div[4]/div/div[2]/img')

    driver.save_screenshot("ss.png")

    location = img.location
    size = img.size
    driver.quit()

    im = Image.open("ss.png")
    left = location['x']
    top = location['y']
    right = location['x'] + size['width']
    bottom = location['y'] + size['height']

    im = im.crop((left, top, right, bottom))

    im.save('ss.png')

def upload_image():
    image = imgur_client.image_upload(path.realpath('./ss.png'), '', '')
    os.remove('ss.png')
    return image["response"]["data"]["link"]


if __name__ == "__main__":
    generate_image()
    url = upload_image()
    print(url)
